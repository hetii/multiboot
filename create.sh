#!/bin/bash

USB_DEVICE=${1}

E2B_URL="https://dl.dropboxusercontent.com/u/7841017/Easy2Boot_V1.16_DPMS.zip"
GRUB4DOS_URL="https://grub4dos-chenall.googlecode.com/files/grub4dos-0.4.5c-2013-07-24.7z"

########## Implementation ##########

E2B_FILE=`basename ${E2B_URL}`
GRUB4DOS_FILE=`basename ${GRUB4DOS_URL}`

WORKSPACE="."

if [ -x "`which sudo`" ];then 
    SUDO=`which sudo`
fi

if [ ! -b "${USB_DEVICE}" ];then
    echo "Missing device: ${USB_DEVICE} - please provide valid one and try again."
    exit
fi

MOUNT_POINT=/tmp/`basename ${USB_DEVICE}`

download_file(){
    FN=`basename ${1}`
    if [ ! -f ${WORKSPACE}/${FN} ];then
        echo "Download file: ${FN}"
        wget -N -P ${WORKSPACE} ${1}
    else
        echo "File ${FN} already exist - No need to download."
    fi
    [ $? -gt 0 ] && echo "Error occured in download_file" && exit
}

check_tools(){
    for tool in ${1}
    do 
        echo "Checking if ${tool} is installed."
        if [ ! -x "`which ${tool}`" ];then
            echo "Tool ${tool} is not installed - please install it manually and try again."
            exit
        fi
    done
}

prepare(){
    check_tools "wget awk 7z umount mount mkfs.vfat unzip"
    download_file ${E2B_URL}
    download_file ${GRUB4DOS_URL}
    echo "Create temporary place where we mount our usb device: ${MOUNT_POINT}."
    mkdir -p ${MOUNT_POINT}

    echo "Umount: ${USB_DEVICE} device."
    ${SUDO} umount ${USB_DEVICE}
    echo "Create FAT32 partition on  ${USB_DEVICE}." 
    ${SUDO} mkfs.vfat -n MULTIBOOT -F 32 ${USB_DEVICE}
    echo "Mount ${USB_DEVICE} to ${MOUNT_POINT}."
    ${SUDO} mount ${USB_DEVICE} ${MOUNT_POINT}
    echo "Extract ${E2B_FILE} to ${MOUNT_POINT}."
    ${SUDO} unzip -qq ${E2B_FILE} -d ${MOUNT_POINT}
    echo "Extract bootlace.com from ${GRUB4DOS_FILE}"
    ${SUDO} 7z l ${GRUB4DOS_FILE} | awk 'match($0, /grub4dos-.+\/bootlace\.com/) { print $6 }' | xargs 7z e -y ${GRUB4DOS_FILE} > /dev/null 
    if [ -x "./bootlace.com" ]; then
        echo "Install new bootloader in ${USB_DEVICE}"
        ${SUDO} ./bootlace.com --time-out=0  `echo ${USB_DEVICE} | sed -e 's/\([0-9]\)*$//g'`
    else
        echo "Bootloader bootlace.com is missing - exit!" 
        exit
    fi
}

prepare
